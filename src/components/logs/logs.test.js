import React from "react";
import { render, screen, fireEvent, waitFor } from "@testing-library/react";
import "@testing-library/jest-dom/extend-expect"; // Import extend-expect to enable toBeInTheDocument
import { Provider } from "react-redux";
import configureStore from "redux-mock-store";
import Logs from "./Logs";
import AddBtn from "../layout/AddBtn";
import AddLogModal from "./AddLogModal";
import { addLog, deleteLog, updateLog } from "../../actions/logActions";

jest.mock("../../actions/logActions", () => {
  const logMessage = "Test message";
  const attention = true;
  const selectedTech = `John Doe`;
  const data = {
    message: logMessage,
    attention: attention,
    tech: selectedTech,
    date: new Date(),
  };
  const updatedLog = {
    id: 2,
    message: "updated log",
    tech: selectedTech,
    attention,
    date: new Date(),
  };
  return {
    addLog: () => {
      return { type: "ADD_LOG", payload: data };
    },
    updateLog: () => ({ type: "UPDATE_LOG", payload: updatedLog }),
    deleteLog: () => ({ type: "DELETE_LOG", payload: "1" }),
    getLogs: () => ({ type: "GET_LOGS", payload: mockLogs }),
  };
});

jest.mock("../../actions/techActions", () => {
  return {
    getTechs: () => ({ type: "GET_TECHS", payload: mockTechs }),
  };
});

const mockLogs = [
  {
    message: "test message",
    attention: false,
    tech: "falakhe sivela",
    date: "2023-08-02T06:28:10.671Z",
    id: 1,
  },
  {
    message: "test message 2",
    attention: true,
    tech: "Jennifer Williams",
    date: "2023-08-02T06:28:26.881Z",
    id: 2,
  },
];

const mockTechs = [
  {
    id: 1,
    firstName: "John",
    lastName: "Doe",
  },
  {
    id: 2,
    firstName: "Sam",
    lastName: "Smith",
  },
];
const log = { logs: mockLogs, current: null, loading: false, error: null };
const tech = {
  techs: mockTechs,
  loading: false,
  error: null,
};
const mockStore = configureStore([]);
const store = mockStore({
  log,
  tech,
});

test("renders the Logs component", async () => {
  render(
    <Provider store={store}>
      <Logs />
    </Provider>
  );

  const headingElement = screen.getByText("System Logs");
  expect(headingElement).toBeInTheDocument();

  await waitFor(() => {
    const logItems = screen.getAllByTestId("log-item");
    expect(logItems).toHaveLength(mockLogs.length);
  });
});

test("renders AddLogModal with TechSelectOptions component", async () => {
  render(
    <Provider store={store}>
      <AddBtn />
      <AddLogModal />
    </Provider>
  );
  const addButton = screen.getByTestId("add-btn");
  fireEvent.click(addButton);
  const modalHeading = screen.getByText("Enter System Log");

  expect(modalHeading).toBeInTheDocument();

  await waitFor(() => {
    const techOptions = screen.getAllByTestId("tech-option");
    expect(techOptions).toHaveLength(mockTechs.length);
  });
});

test("adds a new log on submit", async () => {
  const logMessage = "Test message";
  const attention = true;
  const selectedTech = `${mockTechs[0].firstName} ${mockTechs[0].lastName}`;
  const data = {
    message: logMessage,
    attention: attention,
    tech: selectedTech,
    date: new Date(),
  };
  store.dispatch(addLog(data));
  render(
    <Provider store={store}>
      <AddLogModal />
      <Logs />
    </Provider>
  );

  await waitFor(() => {
    const newLog = screen.getByText(selectedTech);
    expect(newLog).toBeInTheDocument();
  });
});

test("deletes a log when delete button is clicked", async () => {
  store.dispatch(deleteLog("1"));

  render(
    <Provider store={store}>
      <Logs />
    </Provider>
  );

  await waitFor(() => {
    const deletedLog = screen.queryByTestId("log-item-1");
    expect(deletedLog).not.toBeInTheDocument();
  });
});

test("updates a log when edited", async () => {
  const logMessage = "updated log";
  const attention = true;
  const selectedTech = `${mockTechs[0].firstName} ${mockTechs[0].lastName}`;
  const updatedLogData = {
    id: 2,
    message: logMessage,
    tech: selectedTech,
    attention,
    date: new Date(),
  };
  store.dispatch(updateLog(updatedLogData));

  render(
    <Provider store={store}>
      <Logs />
    </Provider>
  );

  await waitFor(() => {
    expect(updatedLogData).not.toEqual(
      store.getState().log.logs.find((log) => log.id === updatedLogData.id)
    );
  });
});
