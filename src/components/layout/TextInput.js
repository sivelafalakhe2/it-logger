import React from "react";
import PropTypes from "prop-types";
const TextInput = ({ label, id, handleChange, value }) => {
  return (
    <div className="input-field">
      <input value={value} onChange={handleChange} id={id} type="text" />
      <label htmlFor={id}>{label}</label>
    </div>
  );
};

TextInput.prototype = {
  label: PropTypes.string.isRequired,
  id: PropTypes.string.isRequired,
  handleChange: PropTypes.func.isRequired,
  value: PropTypes.string.isRequired,
};
export default TextInput;
